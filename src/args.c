/**
 * src/args.c
 *
 * Copyright 2017 Titus Kaptein
 **/

#include "args.h"

#include <argp.h>
#include <stdlib.h>

#define OPT_BPL 1	/* bytes-per-line */
#define OPT_BPB 2	/* bytes-per-block */
#define OPT_ASB 'p'	/* allow-stub-blocks */
#define OPT_RO 'r'	/* read-only */

const char *argp_program_version = "fhex 0.3\nCopyright 2017 Titus Kaptein";
const char *argp_program_bug_address = "<myself@/dev/null>";

/* Program documentation. */
static char doc[] = "fhex -- a lightweight ncurses hex editor.";

/* A description of the arguments we accept. */
static char args_doc[] = "FILENAME";

/* The options we understand. */
static struct argp_option options[] = {
	{"read-only",		OPT_RO, 0,	0, "Don't allow modifying the opened file.", 0 },
	{0, 0, 0, 0, "Display and layout options.", 0},
	{"allow-stub-blocks",	OPT_ASB, 0,	0, "Allow incomplete blocks.", 0 },
	{"bytes-per-line",	OPT_BPL, "NUM",	0, "Max. number of bytes displayed per line.", 0 },
	{"bytes-per-block",	OPT_BPB, "NUM",	0, "Number of bytes grouped in a block.", 0 },
	{ 0, 0, 0, 0, 0, 0 }
};

struct Arguments arguments;

static error_t
args_opt(int key, char *arg, struct argp_state *state);

static struct argp argp = { options, args_opt, args_doc, doc, 0, 0, 0 };

int
args_parse(int argc, char **argv)
{
	arguments.read_only = 0;

	arguments.bytes_per_line = 0;
	arguments.bytes_per_block = 8;
	arguments.allow_partial_blocks = 0;

	arguments.args[0] = NULL;

	/* parse options */
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	return 0;
}

static error_t
args_opt(int key, char *arg, struct argp_state *state)
{
	struct Arguments *args = state->input;

	long tmpl;

	switch (key)
	{
		case OPT_RO:
			args->read_only = 1;
			break;

		case OPT_BPL:
			tmpl = atoi(arg);
			args->bytes_per_line = (tmpl < 0) ? 0 : tmpl;
			break;

		case OPT_BPB:
			tmpl = atoi(arg);
			args->bytes_per_block = (tmpl < 0) ? 0 : tmpl;
			break;

		case OPT_ASB:
			args->allow_partial_blocks = 1;
			break;

		case ARGP_KEY_ARG:
			/* TODO(titus): Maybe add functionality to open
			 * multiple files in different tabs */
			if (state->arg_num >= 1)
			{
				argp_usage(state);
			}
			args->args[state->arg_num] = arg;
			break;

		case ARGP_KEY_END:
			if (state->arg_num < 1)
			{
				argp_usage(state);
			}
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}
