/**
 * src/charutil.c
 *
 * Copyright 2017 Titus Kaptein
 **/

#include "charutil.h"

int
char_to_hex(int ch)
{
	int ret;

	if (ch <= '9')
	{
		ret = ch-'0';
	}
	else if (ch <= 'F')
	{
		ret = ch-'A'+10;
	}
	else
	{
		ret = ch-'a'+10;
	}

	return (ret >= 0 && ret < 16) ? ret : -1;
}
