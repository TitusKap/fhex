/**
 * src/cursor.c
 *
 * Copyright 2017 Titus Kaptein
 **/

#include "cursor.h"

#include <ncurses.h>

#include "args.h"
#include "page.h"

void
cursor_page_pull(Page *page)
{
	unsigned int bpl = page_bytes_per_line(page);

	if (page->cursor < page->page_start)
	{
		page->page_start = (page->cursor/bpl) * bpl;
	}
	else if (page->cursor > page->page_start + (LINES-3)*bpl)
	{
		page->page_start = ((page->cursor/bpl)-(LINES-3)) * bpl;
	}
}

void
cursor_update(Page *page)
{
	uint32_t bytes_per_line = page_bytes_per_line(page);
	int byte_in_line = page->cursor%bytes_per_line;

	int x = 8+page->space+byte_in_line*3;
	if (arguments.bytes_per_block != 0)
	{
		x += byte_in_line/arguments.bytes_per_block;
	}

	int y = 1+(page->cursor-page->page_start)/bytes_per_line;

	if (page->nibble)
	{
		++x;
	}

	move(y,x);
}

void
cursor_set(Page *page, size_t cursor)
{
	if (cursor > span_filesize(page))
	{
		cursor = span_filesize(page);
	}

	page->cursor = cursor;
	cursor_page_pull(page);
}

void
cursor_up(Page *page)
{
	uint32_t bpl = page_bytes_per_line(page);
	if (page->cursor < bpl)
	{
		page->cursor = 0;
	}
	else
	{
		page->cursor -= bpl;
	}
	page->nibble = 0;
	cursor_page_pull(page);
}

void
cursor_down(Page *page)
{
	uint32_t bpl = page_bytes_per_line(page);
	if (page->cursor+bpl > span_filesize(page))
	{
		page->cursor = span_filesize(page);
	}
	else
	{
		page->cursor += bpl;
	}
	page->nibble = 0;
	cursor_page_pull(page);
}

void
cursor_left(Page *page)
{
	if (page->cursor != 0)
	{
		--page->cursor;
	}
	page->nibble = 0;
	cursor_page_pull(page);
}

void
cursor_right(Page *page)
{
	if (page->cursor != span_filesize(page))
	{
		++page->cursor;
	}
	page->nibble = 0;
	cursor_page_pull(page);
}

