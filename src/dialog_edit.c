/**
 * src/dialog_edit.c
 *
 * Copyright 2017 Titus Kaptein
 **/

#include "dialog_edit.h"

#include <string.h>
#include <ctype.h>

void
dialogedit__left(DialogEdit *de)
{
	if (de->cursor > 0)
	{
		--de->cursor;
	}
}

void
dialogedit__right(DialogEdit *de)
{
	if (de->cursor < de->length && de->data[de->cursor] != '\0')
	{
		++de->cursor;
	}
}

void
dialogedit__insert(DialogEdit *de, int ch)
{
	if (de->cursor < de->length-1)
	{
		de->data[de->cursor] = (uint8_t)ch;
		dialogedit__right(de);
	}
}

void
dialogedit__delete(DialogEdit *de)
{
	if (de->cursor < de->length)
	{
		uint8_t *start = de->data+de->cursor;
		memmove(start, start+1, de->length-de->cursor);
		de->data[de->length-1] = 0x00;
	}
}

DialogEdit *
dialogedit_create(enum DialogEditType type, size_t length, size_t bup, size_t blo)
{
	DialogEdit *ret = malloc(sizeof(DialogEdit));
	ret->type = type;
	ret->length = length+1;
	ret->cursor = 0;

	ret->data = calloc(1,length);

	ret->bound_upper = bup;
	ret->bound_lower = blo;

	return ret;
}

void
dialogedit_destroy(DialogEdit *de)
{
	free(de->data);
	free(de);
}

int
dialogedit_input(DialogEdit *de, int ch)
{
	switch (de->type)
	{
		case DIALOGEDIT_DIGIT:
			if (isdigit(ch))
			{
				dialogedit__insert(de, ch);
				return 0;
			}
			break;

		case DIALOGEDIT_XDIGIT:
			if (isxdigit(ch))
			{
				if (ch >= 'a')
				{
					dialogedit__insert(de, ch-32);
				}
				else
				{
					dialogedit__insert(de, ch);
				}
				return 0;
			}
			break;

		case DIALOGEDIT_PRINT:
			if (isprint(ch))
			{
				dialogedit__insert(de, ch);
				return 0;
			}
			break;
	}

	switch (ch)
	{
		case KEY_LEFT:
			dialogedit__left(de);
			return 0;

		case KEY_RIGHT:
			dialogedit__right(de);
			return 0;
			
		case KEY_ENTER:
			return 0;

		case KEY_BACKSPACE:
			dialogedit__left(de);
			dialogedit__delete(de);
			return 0;

		case KEY_DC:
			dialogedit__delete(de);
			return 0;
	}

	return -1;
}

void
dialogedit_draw(DialogEdit *de, WINDOW *win, int y, int x)
{
	wattron(win, COLOR_PAIR(3));
	mvwprintw(win,y,x,"%-*s",de->length-1,de->data);
	wattroff(win, COLOR_PAIR(3));
	wrefresh(win);
}
