/**
 * src/span.c
 *
 * Copyright 2017 Titus Kaptein
 **/

#include "span.h"

#include <ncurses.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "page.h"
#include "colors.h"
#include "args.h"

int
span__map_file(const char *filename, Buffer *buf)
{
	int fd;
	struct stat sb;

	fd = open(filename, O_RDONLY);
	if (fd == -1)
	{
		return -1;
	}

	if (fstat(fd, &sb) == -1)
	{
		close(fd);
		return -1;
	}

	buf->size = sb.st_size;
	buf->data = mmap(NULL, buf->size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (buf->data == (void*)MAP_FAILED)
	{
		close(fd);
		return -1;
	}

	return 0;
}

size_t
span__new_buffer(SpanTable *st, size_t size)
{
	st->buffer_size += 1;
	st->buffer_data = realloc(st->buffer_data, st->buffer_size*sizeof(Buffer));

	size_t buf = st->buffer_size-1;

	st->buffer_data[buf].size = size;
	st->buffer_data[buf].data = malloc(size);

	return buf;
}

int
span__map_cursor(Page *page, size_t cursor, Span **rspan, size_t *roff)
{
	Span *cur = page->spantable.first;

	while (cur != NULL)
	{
		if (cursor < cur->length)
		{
			break;
		}

		cursor -= cur->length;
		cur = cur->next;
	}

	*rspan = cur;
	*roff = cursor;
	return 0;
}

int
span__clear(Page *page)
{
	SpanTable *st = &page->spantable;

	munmap(st->buffer_data[0].data, st->buffer_data[0].size);

	for (size_t i=1; i<st->buffer_size; ++i)
	{
		Buffer *buf = &st->buffer_data[i];
		free(buf->data);
	}
	free(st->buffer_data);

	Span *cur = st->first;
	while (cur != NULL)
	{
		Span *next = cur->next;
		free(cur);
		cur = next;
	}
	st->first = NULL;
	st->last = NULL;

	return 0;
}

int
span__delete_if_empty(Page *page, Span *span)
{
	if (span->length != 0)
	{
		return 0;
	}

	if (span->prev == NULL)
	{
		page->spantable.first = span->next;
	}
	else
	{
		span->prev->next = span->next;
	}

	if (span->next == NULL)
	{
		page->spantable.last = span->prev;
	}
	else
	{
		span->next->prev = span->prev;
	}

	/* TODO(titus): don't delete span, save for undo/redo feature */
	free(span);

	return 0;
}

int
span_init(Page *page)
{
	SpanTable *st = &page->spantable;

	st->buffer_size = 1;
	st->buffer_data = malloc(sizeof(Buffer));

	if (span__map_file(page->filename, &st->buffer_data[0]) == -1)
	{
		return -1;
	}

	st->first = malloc(sizeof(Span));
	st->last = st->first;

	st->first->buffer = 0;
	st->first->offset = 0;
	st->first->length = st->buffer_data[0].size;
	st->first->next = NULL;
	st->first->prev = NULL;

	return 0;
}

int
span_append(Page *page, uint8_t ch)
{
	Span *last = page->spantable.last;

	if (last->buffer == 0)
	{
		size_t buf = span__new_buffer(&page->spantable, 1);
		page->spantable.buffer_data[buf].data[0] = ch;
		
		Span *add = malloc(sizeof(Span));
		add->length = 1;
		add->offset = 0;
		add->buffer = buf;

		add->prev = last;
		add->next = NULL;

		last->next = add;
		page->spantable.last = add;
	}
	else
	{
		Buffer *buf = &page->spantable.buffer_data[last->buffer];
		buf->size += 1;
		buf->data = realloc(buf->data, buf->size);
		buf->data[buf->size-1] = ch;
		++last->length;
	}

	return 0;
}

int
span_insert(Page *page, size_t pos, uint8_t ch)
{
	Span *cur;
	size_t off;

	span__map_cursor(page, pos, &cur, &off);

	if (cur == NULL)
	{
		if (off == 0)
		{
			return span_append(page, ch);
		}

		return -1;
	}

	size_t buf = span__new_buffer(&page->spantable, 1);
	page->spantable.buffer_data[buf].data[0] = ch;

	Span *s0 = malloc(sizeof(Span));
	Span *s1 = malloc(sizeof(Span));
	Span *s2 = malloc(sizeof(Span));

	s0->buffer = cur->buffer;
	s0->offset = cur->offset;
	s0->length = off;
	s0->prev = cur->prev;
	s0->next = s1;

	s1->buffer = buf;
	s1->offset = 0;
	s1->length = 1;
	s1->prev = s0;
	s1->next = s2;

	s2->buffer = cur->buffer;
	s2->offset = cur->offset + off;
	s2->length = cur->length - off;
	s2->prev = s1;
	s2->next = cur->next;

	/* replace cur with s0-s2 */
	if (cur->prev == NULL)
	{
		page->spantable.first = s0;
	}
	else
	{
		cur->prev->next = s0;
	}

	if (cur->next == NULL)
	{
		page->spantable.last = s2;
	}
	else
	{
		cur->next->prev = s2;
	}

	span__delete_if_empty(page, s0);
	span__delete_if_empty(page, s1);
	span__delete_if_empty(page, s2);

	/* TODO(titus): instead of freeing, save cur in a history when 
	 * implementing the undo-redo feature */
	free(cur);

	return 0;
}

int
span_edit(Page *page, size_t pos, uint8_t ch)
{
	Span *cur;
	size_t off;
	span__map_cursor(page, pos, &cur, &off);

	if (cur == NULL)
	{
		if (off == 0)
		{
			return span_append(page, ch);
		}

		return -1;
	}

	if (cur->buffer == 0)
	{
		span_delete(page, pos);
		span_insert(page, pos, ch);
	}
	else
	{
		/* edit the buffer of the current span */
		Buffer *b = &page->spantable.buffer_data[cur->buffer];
		b->data[off] = ch;

		span__delete_if_empty(page, cur);
	}

	return 0;
}

int
span_delete(Page *page, size_t pos)
{
	Span *cur;
	size_t off;
	span__map_cursor(page, pos, &cur, &off);

	if (cur == NULL)
	{
		return -1;
	}

	if (cur->buffer == 0)
	{
		/* TODO(titus): optimize by checking if the delete appears
		 * between two spans, so that we just need to alter one span */

		Span *s0 = malloc(sizeof(Span));
		Span *s1 = malloc(sizeof(Span));

		s0->buffer = cur->buffer;
		s0->length = off;
		s0->offset = cur->offset;
		s0->prev = cur->prev;
		s0->next = s1;

		s1->buffer = cur->buffer;
		s1->length = cur->length-(off+1);
		s1->offset = cur->offset+off+1;
		s1->prev = s0;
		s1->next = cur->next;

		if (cur->prev == NULL)
		{
			page->spantable.first = s0;
		}
		else
		{
			cur->prev->next = s0;
		}

		if (cur->next == NULL)
		{
			page->spantable.last = s1;
		}
		else
		{
			cur->next->prev = s1;
		}

		span__delete_if_empty(page, s0);
		span__delete_if_empty(page, s1);

		/* TODO(titus): instead of freeing, save cur in a history when 
		 * implementing the undo-redo feature */
		free(cur);
	}
	else
	{
		/* TODO(titus): optimize by checking if this delete results in
		 * an empty span and if so, delete it on the spot */
		Buffer *b = &page->spantable.buffer_data[cur->buffer];
		memmove(b->data+off,b->data+off+1,b->size-off-1);
		--b->size;
		--cur->length;

		span__delete_if_empty(page, cur);
	}

	return 0;
}

size_t
span_filesize(Page *page)
{
	/* TODO(titus): cache filesize and only re-calculate when changes to
	 * the spantable are made! */

	size_t size = 0;	
	Span *cur = page->spantable.first;

	while (cur != NULL)
	{
		size += cur->length;
		cur = cur->next;
	}

	return size;
}

size_t
span_load(Page *page, size_t start, size_t count, int *data)
{
	Span *cur;
	size_t off;
	span__map_cursor(page, start, &cur, &off);

	/* fill data array */
	size_t it_data = 0;
	size_t it_span = off;

	while (it_data < count && cur != NULL)
	{
		while (it_span < cur->length && it_data < count)
		{
			Buffer *buf = &page->spantable.buffer_data[cur->buffer];
			int ch = buf->data[cur->offset+it_span];

			if (cur->buffer != 0)
			{
				ch |= COLOR_PAIR(COLOR_HEX_EDITED);
			}

			data[it_data] = ch;

			++it_data;
			++it_span;
		}

		cur = cur->next;
		it_span = 0;
	}

	return it_data;
}

int
span_save(Page *page)
{
	if (arguments.read_only)
	{
		page_message(page, COLOR_RED, "Can't save in read-only mode!");
		return -1;
	}

	size_t count_bytes = 0;
	size_t count_new = 0;
	Span *cur = page->spantable.first;

	char *tmpname = malloc(strlen(page->filename)+6);
	sprintf(tmpname, ".%s.swp", page->filename);

	FILE *fp = fopen(tmpname, "w");
	if (fp == NULL)
	{
		goto err_fopen;
	}

	while (cur != NULL)
	{
		Buffer *buf = &page->spantable.buffer_data[cur->buffer];
		fwrite(buf->data+cur->offset, 1, cur->length, fp);

		count_bytes += cur->length;
		if (cur->buffer != 0)
		{
			count_new += cur->length;
		}

		cur = cur->next;
	}

	fclose(fp);

	char *oldname = malloc(strlen(page->filename)+6);
	sprintf(oldname, ".%s.old", page->filename);

	if (rename(page->filename, oldname) != 0)
	{
		goto err_rename;
	}
	if (rename(tmpname, page->filename) != 0)
	{
		goto err_rename;
	}

	free(tmpname);
	free(oldname);

	page_message(page, COLOR_GREEN, "Successfully wrote %lu bytes (%lu new"
			") to '%s'.",count_bytes, count_new, page->filename);

	/* clear the entire spantable */
	span__clear(page);
	span_init(page);

	return 0;

err_rename:
	free(oldname);

err_fopen:
	free(tmpname);
	page_message(page, COLOR_RED, "Failed to save file '%s'!", 
			page->filename);
	return -1;
}

int
span_info(Page *page)
{
	Span *cur = page->spantable.first;

	fprintf(stderr, "\nSpan: {buffer\toffset\tlength\t}\n");

	while (cur != NULL)
	{
		fprintf(stderr, "Span: {%lu\t%lu\t%lu\t}\n", cur->buffer, 
				cur->offset, cur->length);

		cur = cur->next;
	}

	return 0;
}
