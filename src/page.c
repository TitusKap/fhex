/**
 * src/page.c
 *
 * Copyright 2017 Titus Kaptein
 **/

#include "page.h"

#include <ncurses.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "cursor.h"
#include "dialogs.h"
#include "charutil.h"

#include "args.h"

int
page__hexinput(Page *page, int ch)
{
	ch = char_to_hex(ch);

	if (page->insert && !page->nibble)
	{
		span_insert(page, page->cursor, ((ch & 0x0F) << 4));
		mvprintw(LINES-1,0,"old:%2.2X new:%2.2X\n", 0, ch);
	}
	else
	{
		int old;
		int new;
		span_load(page, page->cursor, 1, &old);
		old = old & 0xFF;
		if (page->nibble)
		{
			new = (old & 0xF0) | (ch & 0x0F);
		}
		else
		{
			new = (old & 0x0F) | (ch << 4);
		}
		span_edit(page, page->cursor, new);
		mvprintw(LINES-1,0,"old:%2.2X new:%2.2X\n", old, new);
	}

	/* move cursor after input */
	if (page->nibble)
	{
		cursor_right(page);
	}
	else
	{
		page->nibble = 1;
	}

	return 0;
}

unsigned int
page_bytes_per_line(Page *page)
{
	unsigned int blocks_per_line;
	unsigned int bytes_per_line;
	int space = 2+COLS-(8+page->space*2);

	if (arguments.bytes_per_block != 0)
	{
		blocks_per_line = space/(arguments.bytes_per_block*4+1);

		bytes_per_line = blocks_per_line*arguments.bytes_per_block;
		space -= blocks_per_line*(arguments.bytes_per_block*4+1);

		if (arguments.allow_partial_blocks)
		{
			bytes_per_line += space/4;
		}
	}
	else
	{
		bytes_per_line = (COLS-8-page->space*2)/4;
	}
	
	if (arguments.bytes_per_line != 0 && 
			bytes_per_line > arguments.bytes_per_line)
	{
		bytes_per_line = arguments.bytes_per_line;
	}

	/* exit program if terminal size doesn't allow a single byte per line */
	/* TODO(titus): use a proper way to exit due to error */
	if (bytes_per_line == 0)
	{
		endwin();
		fprintf(stderr, "Can't operate on a terminal this narrow!\n");
		exit(-1);
	}

	return bytes_per_line;
}

int
page_has_unsaved_changes(Page *page)
{
	return !(page->spantable.first == page->spantable.last);
}

int
page_message(Page *page, short color, const char *format, ...)
{
	int ret;

	init_pair(9, color, COLOR_BLACK);

	va_list arg;
	va_start(arg, format);
	ret = vsnprintf(page->message, PAGE_MSG_LENGTH, format, arg);
	va_end(arg);

	return ret;
}

Page *
page_crate(const char *filename)
{
	if (filename == NULL)
	{
		return NULL;
	}

	/* create page object */
	Page *ret = (Page*)malloc(sizeof(Page));
	if (ret == NULL)
	{
		return NULL;
	}

	/* copy filename */
	ret->filename = (char *)malloc(strlen(filename)+1);
	if (ret->filename == NULL)
	{
		goto err_name;
	}
	strcpy(ret->filename, filename);

	if (span_init(ret) == -1)
	{
		goto err_spaninit;
	}

	/* set default values */
	ret->cursor = 0;
	ret->nibble = 0;

	ret->page_start = 0;

	ret->destroy = 0;

	ret->space = 4;

	return ret;

err_spaninit:
	free(ret->filename);

err_name:
	free(ret);
	return NULL;
}

void
page_input(Page *page, int ch)
{
	switch (ch)
	{
		case KEY_F(1):
			/* TODO(titus): display help and info text */
			break;

		case KEY_F(2):
			dialog_goto(page);
			break;

		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
			page__hexinput(page, ch);
			break;

		case KEY_DC:
			span_delete(page, page->cursor);
			break;

		case CTRL('s'):
			span_save(page);
			break;

		case KEY_UP: 
		case 'k':
			cursor_up(page); 
			cursor_update(page);
			break;

		case KEY_DOWN:
		case 'j':
			cursor_down(page);
			cursor_update(page);
			break;

		case KEY_LEFT:
		case 'h':
			cursor_left(page);
			cursor_update(page);
			break;

		case KEY_RIGHT:
		case 'l':
			cursor_right(page);
			cursor_update(page);
			break;

		case KEY_IC:
			page->insert = !page->insert;
			break;

		case 'n':
			page->nibble = !page->nibble;
			cursor_update(page);
			break;

		case KEY_RESIZE:
			cursor_page_pull(page);
			clear();
			break;
		
		case 'q':
			if (page_has_unsaved_changes(page))
			{
				dialog_unsaved(page);
			}
			else
			{
				page->destroy = 1;
			}
			break;

		case CTRL('i'):
			span_info(page);
			break;

		/* debug functionality to emulate a shrinking terminal since
		 * gdb somehow fucks the COL update up ....
		case 's':
			clear();
			COLS -= 1;
			break;
		*/

		default:
			mvprintw(LINES-1, 0, "No key binding for key: (%u) %c", ch, ch);
			break;
	}
}

void
page_draw(Page *page)
{
	clear();

	/* draw header bar */
	/* TODO(titus): find a way to properly shorten filename when running 
	 * into space issues */
	mvaddch(0,0,ACS_HLINE);
	mvprintw(0,1,"[%8.8X/%8.8X]", page->cursor, span_filesize(page));
	mvaddch(0,20,ACS_HLINE);

	size_t name_space = (COLS >= 24) ? COLS-24 : 0;
	char *display_filename = page->filename;
	if (strlen(page->filename) > name_space)
	{
		display_filename += strlen(page->filename) - name_space;
		display_filename += 3;
		mvprintw(0,COLS-3-name_space,"[...%s]", display_filename);
	}
	else
	{
		mvprintw(0,COLS-3-strlen(display_filename),"[%s]", display_filename);
	}

	mvaddch(0,COLS-1,ACS_HLINE);
	
	unsigned int spacers = 21+3+strlen(page->filename);
	if (spacers < (unsigned int)COLS)
	{
		for (size_t i=0; i<COLS-spacers; ++i)
		{
			mvaddch(0,21+i,ACS_HLINE);
		}
	}

	unsigned int bytes_per_line = page_bytes_per_line(page);

	size_t datasize = (LINES-2) * bytes_per_line;

	int *data = (int*)malloc(sizeof(int)*datasize);
	size_t read = span_load(page, page->page_start, datasize, data);

	for (size_t i=0; i<read; ++i)
	{
		/* TODO(titus): maybe extract this byte-to-cursor position mapping
		 * to it's own function since it is also used in cursor_update */
		int byte_in_line = i%bytes_per_line;
		int x1 = 8 + page->space + 3*byte_in_line;
		if (arguments.bytes_per_block != 0)
		{
			x1 += byte_in_line/arguments.bytes_per_block;
		}

		int x2 = COLS-bytes_per_line+byte_in_line;
		int y = 1 + i/bytes_per_line;

		if (i%bytes_per_line == 0)
		{
			mvprintw(y,0,"%8.8X", page->page_start+i);
		}

		int ch = (data[i] & 0xFF);

		attron(data[i] & 0xFFFFFF00);
		mvprintw(y,x1,"%2.2X",ch);

		if (ch < 32 || ch > 126)
		{
			ch = '.';
		}

		mvaddch(y,x2,ch);
		attroff(data[i] & 0xFFFFFF00);
	}

	attron(COLOR_PAIR(9));
	mvprintw(LINES-1, 0, page->message);
	attroff(COLOR_PAIR(9));

	mvprintw(LINES-1,COLS-6,"[%s]",page->insert?"INS":"OVR");

	free(data);
	cursor_update(page);
}
