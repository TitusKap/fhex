/**
 * src/debugutil.c
 *
 * Copyright 2017 Titus Kaptein
 **/

#include "debugutil.h"

#include <ncurses.h>

#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int
_log(const char *filename, uint32_t linenum, enum LogType type, 
		const char *format, ...)
{
	static const char *colors[3] = {
		"\x1B[0m",
		"\x1B[33m",
		"\x1B[31m"
	};

#if DEBUG != 0
	static FILE *logfile = NULL;
	if (logfile == NULL)
	{
		time_t t = time(NULL);
		struct tm tm;
		tm = *localtime(&t);
		char logname[20];
		sprintf(logname, "%.04u%.2u%.2u_%.2u%.2u%.2u.log", 
				1900+tm.tm_year, tm.tm_mon, tm.tm_mday, 
				tm.tm_hour, tm.tm_min, tm.tm_sec);
		logfile = fopen(logname, "a+");
	}
#endif /* DEBUG != 0 */

	size_t len = strlen(format) + strlen(filename) + 25;
	char *newform = (char *)malloc(len);

	sprintf(newform, "%.5s%s (%u): ",colors[type], filename, linenum);
	strcat(newform, format);
	strcat(newform, colors[0]);
	strcat(newform, "\n");

	int done;

	va_list arg1;
	va_start(arg1, format);
	done = vfprintf(stderr, newform, arg1);
	va_end(arg1);

#if DEBUG != 0
	if (logfile != NULL)
	{
		va_list arg2;
		va_start(arg2, format);
		done = vfprintf(logfile, newform, arg2);
		va_end(arg2);
	}
#endif /* DEBUG != 0 */

	free(newform);

	return done;
}

int
_assert(const char *expression, const char *filename, uint32_t linenum)
{
	def_prog_mode();
	endwin();

	_log(filename, linenum, LOG_ERR, "Assertion failed: %s", expression);

	reset_prog_mode();
	refresh();

	return 0;
}
