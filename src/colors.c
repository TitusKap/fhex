/**
 * src/colors.c
 *
 * Copyright 2017 Titus Kaptein
 **/

#include "colors.h"	

#include <ncurses.h>

void
colors_init(void)
{
	init_pair(COLOR_HEX_EDITED, COLOR_YELLOW, COLOR_BLACK);

	init_pair(COLOR_INPUT, 15,241);
	init_pair(COLOR_INPUT_HIGHLIGHT, 121,241);
	init_pair(COLOR_INPUT_DISABLED, COLOR_RED,241);
}
