/**
 * src/main.c
 *
 * Copyright 2017 Titus Kaptein
 **/

#include <ncurses.h>

#include "page.h"

#include "debugutil.h"
#include "colors.h"
#include "args.h"

int
main(int argc, char **argv)
{
	args_parse(argc, argv);

	initscr();
	raw();
	keypad(stdscr, TRUE);
	noecho();
	set_escdelay(25);

	/* TODO(titus): check if terminal supports colors */
	start_color();
	use_default_colors();
	colors_init();

	int ch = 0;
	int run = 1;

	/* TODO(titus): Add a file open dialog when opened without providing
	 * one or more files for opening */

	/* TODO(titus): Create a wrapper to hold multiple buffers */
	Page *page = page_crate(arguments.args[0]);
	if (page == NULL)
	{
		endwin();
		fprintf(stderr, "Couldn't open file: %s\n", argv[1]);
		return -1;
	}

	page_draw(page);

	while (run)
	{
		ch = getch();
		page_input(page, ch);

		if (page->destroy)
		{
			run = 0;
		}

		page_draw(page);
		refresh();
	}

	endwin();

	return 0;
}
