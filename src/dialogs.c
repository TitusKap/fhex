/**
 * src/dialogs.c
 *
 * Copyright 2017 Titus Kaptein
 **/

#include "dialogs.h"

#include <ncurses.h>

#include <ctype.h>

#include "dialog_edit.h"
#include "cursor.h"
#include "page.h"
#include "charutil.h"
#include "colors.h"
#include "args.h"

void
dialog_goto(Page *page)
{
	WINDOW *dlg_win;
	int ch;
	int loop = 1;
	DialogEdit *edit;
	size_t addr = 0;

	dlg_win = newwin(5, 18, LINES/2-3, COLS/2-9);
	box(dlg_win, 0 , 0);

	mvwprintw(dlg_win,1,2,"Goto:");

	wattron(dlg_win, COLOR_PAIR(COLOR_INPUT_HIGHLIGHT));
	mvwprintw(dlg_win,3,2,"Q");
	mvwprintw(dlg_win,3,14,"O");
	wattroff(dlg_win, COLOR_PAIR(COLOR_INPUT_HIGHLIGHT));

	wattron(dlg_win, COLOR_PAIR(COLOR_INPUT));
	mvwprintw(dlg_win,3,3,"uit");
	mvwprintw(dlg_win,3,15,"k");
	wattroff(dlg_win, COLOR_PAIR(COLOR_INPUT));

	wmove(dlg_win, 1,8);

	/* TODO(titus): Maybe edit this code to allow for gotos for files 
	 * larger than 4GB/8 hex digits in the future */
	edit = dialogedit_create(DIALOGEDIT_XDIGIT, 8, 0, 0);

	while (loop)
	{
		dialogedit_draw(edit, dlg_win, 1, 8);
		wmove(dlg_win, 1,8+edit->cursor);

		wrefresh(dlg_win);
		ch = getch();

		if (!dialogedit_input(edit, ch))
		{
			continue;
		}
		
		switch (ch)
		{
			case 27:
			case 'q':
			case 'c':
			case 'C':
				loop = 0;
				break;

			case 10:
			case 'o':
			case 'O':
				for (size_t it=0; it<edit->length && edit->data[it]; ++it)
				{
					addr *= 16;
					addr += char_to_hex(edit->data[it]);
				}
				cursor_set(page, addr);

				page_message(page, COLOR_WHITE, "Goto: %X", addr);
				loop = 0;
				break;
		}
	}

	wborder(dlg_win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
	wrefresh(dlg_win);
	delwin(dlg_win);
	dialogedit_destroy(edit);
}

void
dialog_unsaved(Page *page)
{
	const int btnpos[3] = { 2, 16, 33 };

	WINDOW *dlg_win;
	int ch;
	int loop = 1;
	size_t cursor = 0;

	dlg_win = newwin(5, 39, LINES/2-5/2, COLS/2-39/2);
	box(dlg_win, 0 , 0);

	mvwprintw(dlg_win,1,2,"Do you want to save before exiting?");

	wattron(dlg_win, COLOR_PAIR(COLOR_INPUT_HIGHLIGHT));
	mvwprintw(dlg_win,3,btnpos[0],"Q");
	mvwprintw(dlg_win,3,btnpos[1],"C");
	if (arguments.read_only)
	{
		wattron(dlg_win, COLOR_PAIR(COLOR_INPUT_DISABLED));
		mvwprintw(dlg_win,3,btnpos[2],"S");
		wattron(dlg_win, COLOR_PAIR(COLOR_INPUT_DISABLED));
	}
	else
	{
		mvwprintw(dlg_win,3,btnpos[2],"S");
	}
	wattroff(dlg_win, COLOR_PAIR(COLOR_INPUT_HIGHLIGHT));

	wattron(dlg_win, COLOR_PAIR(COLOR_INPUT));
	mvwprintw(dlg_win,3,btnpos[0]+1,"uit");
	mvwprintw(dlg_win,3,btnpos[1]+1,"ancel");
	if (arguments.read_only)
	{
		wattron(dlg_win, COLOR_PAIR(COLOR_INPUT_DISABLED));
		mvwprintw(dlg_win,3,btnpos[2]+1,"ave");
		wattron(dlg_win, COLOR_PAIR(COLOR_INPUT_DISABLED));
	}
	else
	{
		mvwprintw(dlg_win,3,btnpos[2]+1,"ave");
	}
	wattroff(dlg_win, COLOR_PAIR(COLOR_INPUT));

	while (loop)
	{
unsaved_skip:
		wmove(dlg_win,3,btnpos[cursor]);
		wrefresh(dlg_win);
		ch = getch();
		
		switch (ch)
		{
			case 'q':
			case 'Q':
				goto unsaved_quit;

			case 27:
			case 'c':
			case 'C':
				goto unsaved_cancel;

			case 's':
			case 'S':
				goto unsaved_save;

			case KEY_LEFT:
				if (cursor > 0)
				{
					--cursor;
				}
				break;

			case KEY_RIGHT:
				if (cursor < 2)
				{
					++cursor;
				}
				break;

			case 10:
				if (cursor == 0)
				{
					goto unsaved_quit;
				}
				else if (cursor == 1)
				{
					goto unsaved_cancel;
				}
				goto unsaved_save;
		}
	}

unsaved_save:
	if (arguments.read_only)
	{
		goto unsaved_skip;
	}
	span_save(page);

unsaved_quit:
	page->destroy = 1;

unsaved_cancel:
	wborder(dlg_win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
	wrefresh(dlg_win);
	delwin(dlg_win);
}
