/**
 * inc/colors.h
 *
 * Copyright 2017 Titus Kaptein
 **/

#ifndef FHEX_COLORS_H
#define FHEX_COLORS_H

#define COLOR_HEX_EDITED 1
#define COLOR_INPUT 3
#define COLOR_INPUT_HIGHLIGHT 4
#define COLOR_INPUT_DISABLED 5

void
colors_init(void);

#endif /* FHEX_COLORS_H */
