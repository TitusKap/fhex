/**
 * inc/debugutil.h
 *
 * Copyright 2017 Titus Kaptein
 */

#ifndef FHEX_DEBUGUTIL_H
#define FHEX_DEBUGUTIL_H

#include <stdint.h>

enum LogType
{
	LOG_NOTE,
	LOG_WARN,
	LOG_ERR
};

#define log(type,...) _log(__FILE__,__LINE__,type,...)

int
_log(const char *,uint32_t,enum LogType,const char *,...);

#define assert(ex) (void)((ex)||(_assert(#ex,__FILE__,__LINE__),0))

int
_assert(const char *,const char *,uint32_t);

#endif /* FHEX_DEBUGUTIL_H */
