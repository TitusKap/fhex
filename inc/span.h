/**
 * inc/span.h
 *
 * Copyright 2017 Titus Kaptein
 **/

#ifndef FHEX_SPAN_H
#define FHEX_SPAN_H

#include <stddef.h>
#include <stdint.h>

typedef struct Page_t Page;

struct Span_t
{
	struct Span_t *prev;
	struct Span_t *next;

	size_t offset;
	size_t length;
	size_t buffer;
};
typedef struct Span_t Span;

struct Buffer_t
{
	size_t size;
	uint8_t *data;
};
typedef struct Buffer_t Buffer;

struct SpanTable_t
{
	Span *first;
	Span *last;

	size_t buffer_size;
	Buffer *buffer_data;
};
typedef struct SpanTable_t SpanTable;

int
span_init(Page *page);

int
span_insert(Page *page, size_t pos, uint8_t ch);

int
span_edit(Page *page, size_t pos, uint8_t ch);

int
span_delete(Page *page, size_t pos);

size_t
span_filesize(Page *page);

size_t
span_load(Page *page, size_t start, size_t count, int *data);

int
span_save(Page *page);

int
span_info(Page *page);

#endif /* FHEX_SPAN_H */
