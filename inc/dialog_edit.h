/**
 * inc/dialog_edit.h
 *
 * Copyright 2017 Titus Kaptein
 **/

#ifndef FHEX_DIALOG_EDIT_H
#define FHEX_DIALOG_EDIT_H

#include <ncurses.h>

#include <stdint.h>
#include <stdlib.h>

enum DialogEditType
{
	DIALOGEDIT_DIGIT = 0,
	DIALOGEDIT_XDIGIT = 1,
	DIALOGEDIT_PRINT = 2
};

struct DialogEdit_t
{
	enum DialogEditType type;
	size_t length;
	size_t cursor;
	uint8_t *data;

	size_t bound_upper;
	size_t bound_lower;
};
typedef struct DialogEdit_t DialogEdit;

DialogEdit *
dialogedit_create(enum DialogEditType type, size_t length, size_t bup, size_t blo);

void
dialogedit_destroy(DialogEdit *de);

int
dialogedit_input(DialogEdit *de, int ch);

void
dialogedit_draw(DialogEdit *de, WINDOW *win, int y, int x);

#endif /* FHEX_DIALOG_EDIT_H */
