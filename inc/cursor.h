/**
 * inc/cursor.h
 *
 * Copyright 2017 Titus Kaptein
 **/

#ifndef FHEX_CURSOR_H
#define FHEX_CURSOR_H

#include <stddef.h>

typedef struct Page_t Page;

void
cursor_page_pull(Page *page);

void
cursor_update(Page *page);

void
cursor_set(Page *page, size_t cursor);

void
cursor_up(Page *page);

void
cursor_down(Page *page);

void
cursor_left(Page *page);

void
cursor_right(Page *page);

#endif /* FHEX_CURSOR_H */
