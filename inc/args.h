/**
 * inc/args.h
 *
 * Copyright 2017 Titus Kaptein
 **/

#ifndef FHEX_ARGS_H
#define FHEX_ARGS_H

struct Arguments
{
	char *args[1];
	int read_only;

	unsigned int bytes_per_line;
	unsigned int bytes_per_block;
	int allow_partial_blocks;
};

extern struct Arguments arguments;

int
args_parse(int argc, char **argv);

#endif /* FHEX_ARGS_H */
