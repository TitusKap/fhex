/**
 * inc/page.h
 *
 * Copyright 2017 Titus Kaptein
 **/

#ifndef FHEX_PAGE_H
#define FHEX_PAGE_H

#ifndef CTRL
#define CTRL(c) ((c) & 037)
#endif

#define PAGE_MSG_LENGTH 100

#include <stddef.h>

#include "span.h"
#include "cursor.h"

struct Page_t
{
	char *filename;
	
	size_t cursor;
	size_t page_start;

	int nibble;
	int insert;

	char message[PAGE_MSG_LENGTH];

	/* TODO(titus): combine bools here into a flags variable and declare
	 * an enum for bitflags */
	int destroy;

	SpanTable spantable;

	/* display options */
	size_t space;
};
typedef struct Page_t Page;

unsigned int 
page_bytes_per_line(Page *page);

int
page_has_unsaved_changes(Page *page);

int
page_message(Page *page, short color, const char *format, ...);

Page *
page_crate(const char *filename);

void
page_input(Page *page, int ch);

void
page_draw(Page *page);

#endif /* FHEX_PAGE_H */
