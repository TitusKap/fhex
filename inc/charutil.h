/**
 * inc/charutil.h
 *
 * Copyright 2017
 **/

#ifndef FHEX_CHARUTIL_H
#define FHEX_CHARUTIL_H

int
char_to_hex(int ch);

#endif /* FHEX_CHARUTIL_H */
