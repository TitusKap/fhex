/**
 * inc/dialogs.h
 *
 * Copyright 2017 Titus Kaptein
 **/

#ifndef FHEX_DIALOGS_H
#define FHEX_DIALOGS_H

typedef struct Page_t Page;

void
dialog_goto(Page *page);

void
dialog_unsaved(Page *page);

#endif /* FHEX_DIALOGS_H */
