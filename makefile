CC = gcc

SRCDIR = src
INCDIR = inc
OBJDIR = obj

EXE = bin/fhex

SRC = $(wildcard $(SRCDIR)/*.c)
OBJ = $(SRC:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

CFLAGS = -std=c11 -Wpedantic -Wall -Wextra -Wshadow -I$(INCDIR)
LDFLAGS = -lncurses

VPATH = $(SRCDIR)

.PHONY: clean debug release

debug: CFLAGS += -ggdb3 -O0 -D DEBUG=1
debug: $(EXE)

release: CFLAGS += -Werror -O2
release: $(EXE)

$(OBJ): | $(OBJDIR)

$(EXE): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

$(OBJDIR)/%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	-rm $(EXE) $(OBJ)
	-rm -r $(OBJDIR)

$(OBJDIR):
	mkdir $@
